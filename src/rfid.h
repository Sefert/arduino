#ifndef RFID_H
#define RFID_H

#include <stdio.h>

typedef struct rfid_card {
    uint8_t uid[10];
    uint8_t uid_size;
    char *holder_name;
    struct rfid_card *next;
} rfid_card_t;

extern uint8_t detect_card(void);
extern rfid_card_t *cards;
extern rfid_card_t *search_card(uint8_t *rfid_uid);
#endif /* RFID_H */
