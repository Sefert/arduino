#include "hmi.h"
#include <avr/pgmspace.h>

#define VER_FW      "Version: " FW_VERSION " built on: " __DATE__ " "__TIME__"\r\n"
#define VER_LIBC    "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"

const char ver_fw[] PROGMEM =  VER_FW;
const char ver_libc[] PROGMEM = VER_LIBC;
const char stud_name[] PROGMEM = "Marko Linde";
const char console_hello[] PROGMEM = "Console Started.\r\n";
const char console_message[] PROGMEM = "\
Use backspace to delete entry and enter to confirm.\r\n\
Arrow key's, scroll and del do not work currently.\r\n";
const char ask_month[] PROGMEM = "Enter Month name first letter >";
const char PICC_DETECT_ERR[] PROGMEM = "Card not found!";
const char DOOR_ACCESS_DENIED[] PROGMEM = "Access Denied!";

const char help_cmd[] PROGMEM = "help";
const char help_help[] PROGMEM = "Get help";
const char ver_cmd[] PROGMEM = "version";
const char ver_help[] PROGMEM = "Print FW version";
const char example_cmd[] PROGMEM = "example";
const char example_help[] PROGMEM =
    "Prints out all provided 3 arguments Usage: example <argument> <argument> <argument>";
const char banner_cmd[] PROGMEM = "banner";
const char banner_help[] PROGMEM = "Print ARDUINORAMA banner";
const char month_cmd[] PROGMEM = "month";
const char month_help[] PROGMEM =
    "Print and display matching months by entered letters from beginning Usage: month<letters>";
const char rfid_read_uid_cmd[] PROGMEM = "read-uid";
const char rfid_read_uid_help[] PROGMEM = "Read MIFARE card and print Card ID";
const char mem_stat_cmd[] PROGMEM = "mem";
const char mem_stat_help[] PROGMEM =
    "Print memory usage and change compared to previous call";
const char rfid_add_cmd[] PROGMEM = "add";
const char rfid_add_help[] PROGMEM =
    "Add MIFARE card to list. Usage: add <card uid in HEX> <card holder name>";
const char rfid_rm_cmd[] PROGMEM = "rm";
const char rfid_rm_help[] PROGMEM =
    "Remove MIFARE card from list Usage: rm <card uid in HEX>";
const char rfid_print_cmd[] PROGMEM = "print";
const char rfid_print_help[] PROGMEM = "Print stored access card list";

PGM_P const month_names[] PROGMEM = {month_1, month_2, month_3, month_4, month_5,
                                     month_6, month_7, month_8, month_9, month_10,
                                     month_11, month_12
                                    };

const char month_1[] PROGMEM = "January";
const char month_2[] PROGMEM = "February";
const char month_3[] PROGMEM = "March";
const char month_4[] PROGMEM = "April";
const char month_5[] PROGMEM = "May";
const char month_6[] PROGMEM = "June";
const char month_7[] PROGMEM = "July";
const char month_8[] PROGMEM = "August";
const char month_9[] PROGMEM = "September";
const char month_10[] PROGMEM = "October";
const char month_11[] PROGMEM = "November";
const char month_12[] PROGMEM = "December";

PGM_P const banner[] PROGMEM = {ship_1, ship_2, ship_3, ship_4, ship_5, ship_6,
                                ship_7, ship_8, ship_9, ship_10, ship_11,
                                ship_12, ship_13, ship_14
                               };

const char ship_1[] PROGMEM = "                     `. ___";
const char ship_2[] PROGMEM =
    "                    __,' __`.                _..----....____";
const char ship_3[] PROGMEM =
    "        __...--.'``;.   ,.   ;``--..__     .'    ,-._    _.-'";
const char ship_4[] PROGMEM =
    "  _..-''-------'   `'   `'   `'     O ``-''._   (,;') _,'";
const char ship_5[] PROGMEM =
    ",'________________                          \\`-._`-','";
const char ship_6[] PROGMEM =
    " `._              ```````````------...___   '-.._'-:";
const char ship_7[] PROGMEM =
    "    ```--.._      ,.                     ````--...__\\-.";
const char ship_8[] PROGMEM =
    "            `.--. `-`  --ARDUINORAMA--      ____    |  |`";
const char ship_9[] PROGMEM =
    "              `. `.                       ,'`````.  ;  ;`";
const char ship_10[] PROGMEM =
    "                `._`.        __________   `.      \\'__/`";
const char ship_11[] PROGMEM =
    "                   `-:._____/______/___/____`.     \\  `";
const char ship_12[] PROGMEM =
    "                               |       `._    `.    \\";
const char ship_13[] PROGMEM =
    "                               `._________`-.   `.   `.___";
const char ship_14[] PROGMEM =
    "                                             SSt  `------'`";
