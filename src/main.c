#include <avr/io.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <time.h>
#include <avr/interrupt.h>

#include "../lib/andygock_avr_uart/uart.h"
#include "hmi.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/hd44780_111/hd44780_settings.h"
#include "print_helper.h"
#include "cli_microrl.h"
#include "rfid.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"


#define UART_BAUD           9600
#define UART_STATUS_MASK    0x00FF
#define my_free(any)        do {free(any); any = NULL;} while (0)

/* Create global microrl object and pointer on it */
microrl_t rl;
microrl_t *prl = &rl;

typedef enum {
    door_opening,
    door_open,
    door_closing,
    door_closed
} door_state_t;
typedef enum {
    display_name,
    display_access_denied,
    display_clear,
    display_no_update,
} display_state_t;

//access_state_t access;
door_state_t door_state;
display_state_t display_state;
uint8_t read_uid[10];

/* Function declaration */
static inline void init_rfid_reader(void);
static inline void init_leds(void);
static inline void init_lcd(void);
static inline void init_err(void);
static inline void init_console();
static inline void init_con_uart(void);
static inline void init_sys_timer(void);
static inline void heartbeat(void);
static inline void status_refresh();
void display(time_t now, char *display_name_str);
void door(time_t now);


static inline void init_rfid_reader(void)
{
    /* Init RFID-RC522 */
    MFRC522_init();
    PCD_Init();
}

static inline void init_leds(void)
{
    /* Set port A pin 23,25,27 for output */
    DDRA |= _BV(DDA1) | _BV(DDA3) | _BV(DDA5);
    DDRB |= _BV(DDB7);
    PORTB &= ~_BV(BOARD_LED);
}

static inline void init_lcd(void)
{
    lcd_init();
    lcd_goto(LCD_ROW_1_START);
    lcd_puts_P(stud_name);
}

/* Init error console as stderr in UART1 and print user code info */
static inline void init_err(void)
{
    uart1_puts_p(ver_fw);
    uart1_puts_p(ver_libc);
}

static inline void init_console()
{
    uart0_puts_p(console_hello);
    uart0_puts_p(stud_name);
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(console_message);
    // Call init with ptr to microrl instance and print callback
    microrl_init(prl, uart0_puts);
    // Set callback for execute
    microrl_set_execute_callback(prl, cli_execute);
}

static inline void init_con_uart(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
}

static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 12499; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}

static inline void status_refresh()
{
    time_t now = time(NULL);
    Uid uid;
    rfid_card_t *card;

    if (detect_card()) {
        PICC_ReadCardSerial(&uid);
        card = search_card(uid.uidByte);

        if (card && memcmp(uid.uidByte, card->uid, card->uid_size) != 0) {
            card = card->next;
        }

        if (card && memcmp(read_uid, card->uid, card->uid_size) != 0) {
            door_state = door_opening;
            display_state = display_name;
            memcpy(read_uid, card->uid, card->uid_size);
        } else if (!card) {
            door_state = door_closing;
            display_state = display_access_denied;
        }
    }

    if (door_state != door_closed) {
        door(now);
    }

    if (display_state != display_no_update) {
        display(now, card->holder_name);
    }
}

void door(time_t now)
{
    static time_t door_open_time;

    switch (door_state) {
    case door_opening:
        // Document door open time
        // Unlock door
        door_open_time = now;
        door_state = door_open;
        PORTA |= _BV(LED_GREEN);
        break;

    case door_open:
        if ((now - door_open_time) >= system_time(DOOR_OPEN_IN_SEC)) {
            door_state = door_closing;
        }

        break;

    case door_closing:
        // Lock door
        PORTA &= ~_BV(LED_GREEN);
        memset(read_uid, 0, sizeof(read_uid));
        door_state = door_closed;
        break;

    case door_closed:
        break; // No need to do anything
    }
}

void display(time_t now, char *display_name_str)
{
    static time_t msg_display_time;

    switch (display_state) {
    case display_name:
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        lcd_goto(LCD_ROW_2_START);

        //You need to point display_name_str pointer to read card struct card holder name
        if (display_name_str != NULL) {
            // You need to truncate name display because name can be long
            char lcd_buf[17] ;
            strncpy(lcd_buf, display_name_str, LCD_VISIBLE_COLS);
            lcd_puts(display_name_str);
        } else {
            lcd_puts_P(PSTR("Name read error"));
        }

        msg_display_time = now;
        display_state = display_clear;
        break;

    case display_access_denied:
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        lcd_goto(LCD_ROW_2_START);
        lcd_puts_P(DOOR_ACCESS_DENIED);
        msg_display_time = now;
        display_state = display_clear;
        break;

    case display_clear:
        if ((now - msg_display_time) >= system_time(DISPLAY_MSG_IN_SEC)) {
            lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
            display_state = display_no_update;
        }

        break;

    case display_no_update:
        break;
    }
}

/*200 ms heartbeat*/
static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if ((now - prev_time) >= 5) {
        //Print 1s uptime to uart1git
        ltoa(now / 5, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR("s.\r\n"));
        prev_time = now;
    }
}

void main(void)
{
    door_state = door_closed;
    display_state = display_no_update;
    init_leds();
    init_con_uart();
    init_err();
    init_sys_timer();
    init_console();
    init_rfid_reader();
    init_lcd();
    sei();

    while (1) {
        heartbeat();
        status_refresh();
        // CLI commands are handled in cli_execute()
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}

/* System timer ISR */
ISR(TIMER1_COMPA_vect)
{
    system_tick();
}


