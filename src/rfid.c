#include <stdio.h>
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "rfid.h"
#include <string.h>

rfid_card_t *cards;

uint8_t detect_card(void)
{
    // Not used, but library requires those
    uint8_t bufferATQA[2];
    uint8_t bufferSize = sizeof(bufferATQA);

    if (PICC_IsNewCardPresent()) {
        return 1;
    }

    //Try to wake card
    if (PICC_WakeupA(bufferATQA, &bufferSize) == (STATUS_OK || STATUS_COLLISION)) {
        return 1;
    } else {
        return 0;
    }

    return 0;
}

/*Returns previous card from linked-list*/
rfid_card_t *search_card(uint8_t *rfid_uid)
{
    rfid_card_t *search_card = cards;
    rfid_card_t *previous_card = cards;

    while (search_card != NULL) {
        //https://stackoverflow.com/questions/12595748/how-to-compare-two-unsigned-integers-uint8-t-in-c
        if (memcmp(rfid_uid, search_card->uid, search_card->uid_size) == 0) {
            return previous_card;
        }

        previous_card = search_card;
        search_card = search_card->next;
    }

    return 0;
}
