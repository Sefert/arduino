#ifndef PRINT_HELPER_H
#define PRINT_HELPER_H

#include <stdio.h>
#include <avr/pgmspace.h>
#include "rfid.h"

extern void print_banner(FILE *stream, const char *my_array[], int rows);
extern void print_banner_P(void (*puts_function)(const char *),
                        PGM_P const *my_array, const int rows);
extern void print_bytes(void (*putc_function)(uint8_t data),
                        const uint8_t *array, const size_t len);
extern void hextobin(const char * str, uint8_t * bytes, size_t blen);
extern void print_card(void (*puts_function)(const char *),
                        void (*putc_function)(uint8_t data), rfid_card_t *card);
#endif /* PRINT_HELPER_H */
