#include <avr/pgmspace.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "print_helper.h"
#include "rfid.h"

void print_banner(FILE *stream, const char *my_array[], int rows)
{
    for (int row = 0; row < rows; row++) {
        fprintf(stream, "%s\r\n", my_array[row]);
    }

    fprintf(stream, "\r\n");
}

void print_banner_P(void (*puts_function)(const char *), PGM_P const *my_array,
                    const int rows)
{
    for (int row = 0; row < rows; row++) {
        puts_function((PGM_P) pgm_read_word(&(my_array[row])));
        puts_function(PSTR("\r\n"));
    }

    puts_function(PSTR("\r\n"));
}

void print_bytes(void (*putc_function)(uint8_t data), const uint8_t *array,
                 const size_t len)
{
    for (size_t i = 0; i < len; i++) {
        putc_function((array[i] >> 4) + ((array[i] >> 4) <= 9 ? 0x30 : 0x37));
        putc_function((array[i] & 0x0F) + ((array[i] & 0x0F) <= 9 ? 0x30 : 0x37));
    }
}

int get_raw_int(char c)
{
    if (isalpha(c)) {
        return toupper(c) - 'A' + 10;
    }

    return c - '0';
}

// In inspiration of https://stackoverflow.com/a/23898449/266720
void hextobin(const char * str, uint8_t * bytes, size_t blen)
{
    memset(bytes, 0, blen);

    for (uint8_t pos = 0; pos < strlen(str); pos += 2) {
        bytes[pos / 2] = get_raw_int(str[pos]) << 4 | get_raw_int(str[pos + 1]);
    }
}

void print_card(void (*puts_function)(const char *),
                void (*putc_function)(uint8_t data), rfid_card_t *card)
{
    char print_buf[256] = {0x00};
    puts_function("Card UID: ");
    print_bytes(putc_function, card->uid, card->uid_size);
    sprintf_P(print_buf, PSTR("\r\nUID size: %d\r\nName: %s\r\n"),
              card->uid_size, card->holder_name);
    puts_function(print_buf);
}
