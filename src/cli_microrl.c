#include <string.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr_uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/eriks_freemem/freemem.h"
#include "../lib/andy_brown_memdebug/memdebug.h"
#include "hmi.h"
#include "rfid.h"
#include "cli_microrl.h"
#include "print_helper.h"
#include "../lib/matejx_avr_lib/mfrc522.h"

#define NUM_ELEMS(x)    (sizeof(x) / sizeof((x)[0]))
#define my_free(any)        do {free(any); any = NULL;} while (0)


void cli_mem_stat(const char *const *argv);
void cli_rfid_read_uid(const char *const *argv);
void cli_print_help(const char *const *argv);
void cli_print_banner(const char *const *argv);
void cli_example(const char *const *argv);
void cli_print_ver(const char *const *argv);
void cli_handle_month(const char *const *argv);
void cli_rfid_add(const char *const *argv);
char* to_str(const char * string);
void cli_print_cards(const char *const *argv);
void cli_rfid_rm(const char *const *argv);

typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;

const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {example_cmd, example_help, cli_example, 3},
    {banner_cmd, banner_help, cli_print_banner, 0},
    {month_cmd, month_help, cli_handle_month, 1},
    {mem_stat_cmd, mem_stat_help, cli_mem_stat, 0},
    {rfid_read_uid_cmd, rfid_read_uid_help, cli_rfid_read_uid, 0},
    {rfid_add_cmd, rfid_add_help, cli_rfid_add, 2},
    {rfid_print_cmd, rfid_print_help, cli_print_cards, 0},
    {rfid_rm_cmd, rfid_rm_help, cli_rfid_rm, 1},
};

void cli_rfid_add(const char *const *argv)
{
    //Check max possible uid_size
    if (strlen(argv[1]) > 20) {
        uart0_puts_p(PSTR("UID can't be over 10 bytes\r\n"));
        return;
    }

    rfid_card_t *new_card = malloc(sizeof(rfid_card_t));
    char *str_buf = to_str(argv[2]);
    //Convert uid hex to compressed binary
    hextobin(argv[1], new_card->uid, sizeof(new_card->uid));
    new_card->uid_size = strlen(argv[1]) / 2;
    new_card->holder_name = str_buf;

    //Check if added card exists
    if (search_card(new_card->uid)) {
        uart0_puts_p(PSTR("Card already exists!\r\n"));
        my_free(new_card);
        return;
    }

    uart0_puts_p(PSTR("Added card->"));
    uart0_puts_p(PSTR("\r\n"));
    print_card(uart0_puts, uart0_putc, new_card);
    new_card->next = cards;
    cards = new_card;
}

/*Remove card from heap*/
void cli_rfid_rm(const char *const *argv)
{
    rfid_card_t *previous_card;
    rfid_card_t *next_card;
    uint8_t uid[10];
    hextobin(argv[1], uid, sizeof(uid));
    previous_card = search_card(uid);

    if (previous_card) {
        //Remove end
        if (memcmp(uid, previous_card->next->uid, sizeof(uid)) == 0 &&
                previous_card->next->next == NULL) {
            my_free(previous_card->next->holder_name);
            my_free(previous_card->next);
            //Remove head
        } else if (memcmp(uid, previous_card->uid, previous_card->uid_size) == 0) {
            next_card = cards;
            cards = previous_card->next;
            my_free(next_card->holder_name);
            my_free(next_card);
            //Remove mid
        } else {
            next_card = previous_card->next;
            previous_card->next = next_card->next;
            my_free(next_card->holder_name);
            my_free(next_card);
        }
    } else {
        uart0_puts_p(PSTR("Card not found!\r\n"));
        return;
    }
}

void cli_print_cards(const char *const *argv)
{
    (void) argv;
    char print_buf[4] = {0x00};
    int counter = 0;
    rfid_card_t *current_card = cards;

    if (cards == NULL) {
        uart0_puts_p(PSTR("No cards in list!\r\n"));
        return;
    }

    while (current_card != NULL) {
        sprintf_P(print_buf, PSTR("\r\nCard - %d\r\n"), ++counter);
        uart0_puts(print_buf);
        print_card(uart0_puts, uart0_putc, current_card);
        current_card = current_card->next;
    }
}

char* to_str(const char * string)
{
    char *str_to_ret = (char*)malloc (strlen(string) * sizeof(char) + 1);
    *str_to_ret = '\0';
    strcpy(str_to_ret,  string);
    return str_to_ret;
}

void cli_mem_stat(const char *const *argv)
{
    (void) argv;
    char print_buf[256] = {0x00};
    uint16_t space = freeMem();
    static uint16_t prev_space;
    uart0_puts_p(PSTR("\r\nSpace between stack and heap:\r\n"));
    sprintf_P(print_buf, PSTR("Current  %d\r\nPrevious %d\r\nChange   %d\r\n"),
              space, prev_space, space - prev_space);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nHeap statistics\r\n"));
    sprintf_P(print_buf, PSTR("Used: %u\r\nFree: %u\r\n"), getMemoryUsed(),
              getFreeMemory());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest non freelist block:          %u\r\n"),
              getLargestNonFreeListBlock());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest dynamically allocable block: %u\r\n"),
              getLargestAvailableMemoryBlock());
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nFreelist\r\n"));
    sprintf_P(print_buf, PSTR("Freelist size:             %u\r\n"),
              getFreeListSize());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Blocks in freelist:        %u\r\n"),
              getNumberOfBlocksInFreeList());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest block in freelist: %u\r\n"),
              getLargestBlockInFreeList());
    uart0_puts(print_buf);
    prev_space = space; // Document space change for next call
}

void cli_rfid_read_uid(const char *const *argv)
{
    (void) argv;
    Uid uid;

    if (!detect_card()) {
        uart0_puts_p(PICC_DETECT_ERR);
        return;
    }

    uart0_puts_p(PSTR("Card selected!\r\n"));
    PICC_ReadCardSerial(&uid);
    uart0_puts_p(PSTR("Card type: "));
    uart0_puts(PICC_GetTypeName(PICC_GetType(uid.sak)));
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR("Card UID: "));
    print_bytes(uart0_putc, uid.uidByte, uid.size);
    uart0_puts_p(PSTR(" (size "));
    print_bytes(uart0_putc, &uid.size, sizeof(uid.size));
    uart0_puts_p(PSTR(" bytes)\r\n"));
}

void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("Implemented commands:\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(PSTR("\r\n"));
    }
}

void cli_print_banner(const char *const *argv)
{
    (void) argv;
    print_banner_P(uart0_puts_p, banner, BANNER_ROWS);
}

void cli_example(const char *const *argv)
{
    uart0_puts_p(PSTR("Command had following arguments:\r\n"));

    for (uint8_t i = 1; i < 4; i++) {
        uart0_puts(argv[i]);
        uart0_puts_p(PSTR("\r\n"));
    }
}

void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(ver_fw);
    uart0_puts_p(ver_libc);
}

void cli_handle_month(const char *const *argv)
{
    bool no_months = true;
    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
    lcd_goto(LCD_ROW_2_START);

    for (int month = 0; month < MONTH_COUNT; month++) {
        char month_buf[10];
        strcpy_P(month_buf, (PGM_P) pgm_read_word(&(month_names[month])));

        if (!strncmp(argv[1], month_buf, strlen(argv[1]))) {
            no_months = false;
            uart0_puts(month_buf);
            uart0_puts_p(PSTR("\r\n"));
            lcd_puts(month_buf);
            lcd_putc(' ');
        }
    }

    if (no_months) {
        uart0_puts_p(PSTR(NOMONTH));
        uart0_puts_p(PSTR("\r\n"));
        lcd_puts_P(PSTR(NOMONTH));
        no_months = true;
    }
}

int cli_execute(int argc, const char *const *argv)
{
    // Move cursor to new line. Then user can see what was entered.
    uart0_puts_p(PSTR("\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                uart0_puts_p(
                    PSTR("To few or too many arguments for this command.\r\n\tUse <help>\r\n"));
                return 1;
            }

            // Hand argv over to function via function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p(argv);
            return 0;
        }
    }

    uart0_puts_p(PSTR("Command not implemented.\r\n\tUse <help> to get help.\r\n"));
    return 1;
}
