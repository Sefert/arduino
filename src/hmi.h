#ifndef HMI_H
#define HMI_H

#include <stdio.h>
#include <avr/pgmspace.h>

#define MONTH_COUNT         12
#define BANNER_ROWS         14
#define LED_RED             PORTA1 // Arduino Mega digital pin 23
#define LED_GREEN           PORTA3 // Arduino Mega digital pin 25
#define LED_BLUE            PORTA5 // Arduino Mega digital pin 27
#define BOARD_LED           PORTB7
#define BLINK_DELAY_MS      300
#define NOMONTH             "No such month"
#define DISPLAY_MSG_IN_SEC  5
#define DOOR_OPEN_IN_SEC    3

#define system_time(seconds)   (seconds*5)

extern const char stud_name[];
extern const char console_hello[];
extern const char console_message[];
extern const char ask_month[];
extern const char ver_fw[];
extern const char ver_libc[];
extern const char PICC_DETECT_ERR[];
extern const char DOOR_ACCESS_DENIED[];

extern const char help_cmd[];
extern const char help_help[];
extern const char ver_cmd[];
extern const char ver_help[];
extern const char example_cmd[];
extern const char example_help[];
extern const char banner_cmd[];
extern const char banner_help[];
extern const char month_cmd[];
extern const char month_help[];
extern const char rfid_read_uid_cmd[];
extern const char rfid_read_uid_help[];
extern const char mem_stat_cmd[];
extern const char mem_stat_help[];
extern const char rfid_add_cmd[];
extern const char rfid_add_help[];
extern const char rfid_rm_cmd[];
extern const char rfid_rm_help[];
extern const char rfid_print_cmd[];
extern const char rfid_print_help[];

extern const char month_1[];
extern const char month_2[];
extern const char month_3[];
extern const char month_4[];
extern const char month_5[];
extern const char month_6[];
extern const char month_7[];
extern const char month_8[];
extern const char month_9[];
extern const char month_10[];
extern const char month_11[];
extern const char month_12[];

extern const char ship_1[];
extern const char ship_2[];
extern const char ship_3[];
extern const char ship_4[];
extern const char ship_5[];
extern const char ship_6[];
extern const char ship_7[];
extern const char ship_8[];
extern const char ship_9[];
extern const char ship_10[];
extern const char ship_11[];
extern const char ship_12[];
extern const char ship_13[];
extern const char ship_14[];

extern PGM_P const month_names[];
extern PGM_P const banner[];
#endif /* HMI_H */
